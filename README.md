# README #

###1 Create one table or several tables(noun, verb, pronoun)  

```sql
CREATE TABLE `word` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_bin',
	`count` INT(11) NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;
```

###2 Create procedure:

Procedure select random word from table and increment count field of selected row.

Param IN table_name

Param IN totalCombination = noun_total_rows * verb_total_rows * pronoun_total_rows

Param OUT result Selected word from table_name <br />


```sql
CREATE DEFINER=`root`@`%` PROCEDURE `select_and_update`(
	IN `table_name` VARCHAR(50),
	IN `totalCombination` INT,
	OUT `result` VARCHAR(50)

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN

	SET @table_name = table_name;
	
	SET @sql_text1 = concat('SELECT name INTO @name FROM ', @table_name, ' WHERE count < ', totalCombination, ' ORDER BY RAND() LIMIT 1;');
	PREPARE stmt1 FROM @sql_text1;
	EXECUTE stmt1;
	DEALLOCATE PREPARE stmt1;
	
	SET @sql_text2 = concat('UPDATE ', @table_name, ' SET count = count + 1 WHERE name = "', @name, '"');
	PREPARE stmt2 FROM @sql_text2;
	EXECUTE stmt2;
	DEALLOCATE PREPARE stmt2;
	
	SET result = @name;
	
END
```

###3 Usage:


```sql
CALL select_and_update("word", 500, @word1);
CALL select_and_update("word", 500, @word2);
SELECT CONCAT(@word1, " ", @word2);
```